$fn=64;
x=20;
y=40;
z=3;
boltHoleRadius=2;

difference()
{
	cube ([x,y,z]);
	translate([x/2,y/2.8,-.01])
		cylinder(r=boltHoleRadius,h=z+.02);
}